import React from 'react';
import MainLayout from "../components/layouts/MainLayout";
import Image from "next/image";

const Specialists = () => {
    return (

        <MainLayout>
            <header className="bg-primary text-center py-5 mb-4">
                <div className="container">
                    <h1 className="fw-light text-white">Meet Our Specialists </h1>
                </div>
            </header>
            <div className="container">
                <div className="separator  mb-3">
                    <button className="btn btn-primary py-1 px-5 mb-0">Makeup</button>
                    <hr className="my-1"/>
                </div>
                <div className="row">
                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/TMgQMXoglsM/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">WillFrie Zeus</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/9UVmlIb0wJU/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">Laura  Telli</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/sNut2MqSmds/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">Jeroma Ball</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/7u5mwbu7qLg/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">Rita Mark</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="separator  mb-3">
                    <button className="btn btn-primary py-1 px-5 mb-0">Barbers</button>
                    <hr className="my-1"/>
                </div>
                <div className="row">
                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/TMgQMXoglsM/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">WillFrie Zeus</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/9UVmlIb0wJU/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">Laura  Telli</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/sNut2MqSmds/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">Jeroma Ball</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/7u5mwbu7qLg/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">Rita Mark</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="separator  mb-3">
                    <button className="btn btn-primary py-1 px-5 mb-0">Massage</button>
                    <hr className="my-1"/>
                </div>
                <div className="row">
                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/TMgQMXoglsM/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">WillFrie Zeus</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/9UVmlIb0wJU/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">Laura  Telli</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/sNut2MqSmds/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">Jeroma Ball</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>

                    <div className="col-xl-3 col-md-6 mb-4">
                        <div className="card border-0 shadow">
                            <Image src="https://source.unsplash.com/7u5mwbu7qLg/500x350" width={500} height={350} className="card-img-top" alt="..." />
                            <div className="card-body text-center">
                                <h5 className="card-title mb-0">Rita Mark</h5>
                                <div className="card-text text-black-50 py-2"/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    )

};

export default Specialists;