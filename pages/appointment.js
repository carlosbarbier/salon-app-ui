import React from 'react';
import MainLayout from "../components/layouts/MainLayout";
import NewBooking from "../components/booking/NewBooking";

const Appointment = () => {
const des=`At Salon App, we believe that true beauty comes from within; a result of being healthy and confident; 
taking the time to nurture your body, your skin, yourself. We take an integrated approach to beauty, helping our clients 
discover the best version of themselves`
    return (
        <MainLayout>

            <NewBooking sectionId="appointment"  title="Book a Spot Now"  description={des} />
        </MainLayout>
    )
}

export default Appointment;