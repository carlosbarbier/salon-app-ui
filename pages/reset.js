import React, {useState} from 'react';
import MainLayout from "../components/layouts/MainLayout";
import {useForm} from "react-hook-form";
import {resetPasswordSchema} from "../validation";
import {resetPassword} from "../api/auth";
import {getErrors} from "../utils";
import Router from "next/router";
import ErrorComponent from "../components/common/ErrosComponent";
import {yupResolver} from "@hookform/resolvers/yup";
import Link from "next/link";
import {Divider} from "antd";
import LoadingButton from "../components/common/LoadingButton";
import Notification from "../components/common/Notification";


const ResetPassword = () => {
    const [loading, setLoading] = useState(false)
    const [apiErrs, setApiErrors] = useState(null)
    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(resetPasswordSchema)});
    const [show, setShow] = useState(false)
    const onSubmitResetPassword = async (data) => {
        setLoading(true)
        const payload = {
            confirm_code: data.confirm_code,
            password: data.password,
            email: data.email
        }
        const response = await resetPassword(payload);
        if (response.status !== 200) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            setLoading(false)
            reset({confirm_code: "", password: "", email: ""})
            setShow(true)
            setTimeout(() => {
                Router.push("/login")
            }, 3000)
        }
    };
    return (
        <MainLayout>
            <div className="container d-flex flex-column my-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-4">
                        <div className="card shadow border-0 mb-4 ">
                            <div className="card-body p-6">
                                <div className="mb-4 text-center ">
                                    <h3 className="mb-3 fw-bold ">Reset Password</h3>
                                </div>
                                <form onSubmit={handleSubmit(onSubmitResetPassword)}>
                                    {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                    {show && <Notification type="success"
                                                           message="Password updated successfully"/>}
                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">Email</label>
                                        <input type="email"
                                               className="form-control"  {...register("email")} />
                                        <span className="text-danger">{errors.email?.message}</span>
                                    </div>

                                    <div className="mb-3">
                                        <label htmlFor="name" className="form-label"> New Password</label>
                                        <input type="password"
                                               className="form-control"  {...register("password")} />
                                        <span className="text-danger">{errors.password?.message}</span>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="name" className="form-label">Code</label>
                                        <input type="text"
                                               className="form-control"  {...register("confirm_code")} />
                                        <span className="text-danger">{errors.confirm_code?.message}</span>
                                    </div>
                                    <div>
                                        <div className="d-grid pb-3 mt-2 mb-3">
                                            <div className="d-grid ">
                                                <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                               message="Reset Password"
                                                               load_message="Resetting Password"/>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <Divider style={{fontSize: "12px"}}>Resend Code</Divider>
                                <div className="mt-3 text-center">
                                    <div className="d-grid py-1">
                                        <Link href="/resend">
                                            <a className="btn btn-outline-secondary register_btn">Resend Code</a>
                                        </Link>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export default ResetPassword;