import React from 'react';
import MainLayout from "../components/layouts/MainLayout";
import Link from "next/link";
const About = () => {
    return (

        <MainLayout>
            <div id="about" className="section">
                <div className="section-center">
                    <div className=" bg-surface-secondary" >
                        <div className="container max-w-screen-xl  ">
                            <div className="row justify-content-center">
                                <div className="col-12 col-md-10 col-lg-8 text-center">
                                    <h1 className="ls-tight  mb-5">
                                        <span className="fw-bold"> Our mission: </span>
                                        <br/> <span className="fs-1"> Your</span> <span
                                        className="text-secondary fw-bold "> Health </span> and <span
                                        className="text-secondary fw-bold ">Wellness</span>
                                    </h1>

                                    <p className="lead mb-10">
                                        Combining the science of beauty with excellence in customer care, to deliver exceptional results.
                                    </p>

                                    <div className="my-5">
                                        <Link href="/appointment">
                                            <a  className="btn btn-lg btn-primary">
                                                Book Appointment
                                            </a>
                                        </Link>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    );
};
export default About;