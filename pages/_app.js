import 'bootstrap-icons/font/bootstrap-icons.css'
import '../styles/scss/bootstrap.scss'

import '../styles/scss/styles.scss'

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />
}

export default MyApp
