import Document, {Head, Html, Main, NextScript} from "next/document";
import React from "react";

class MyDocument extends Document {
    render() {
        return (
            <Html lang="eng">
                <Head>
                    <meta charSet="UTF-8"/>
                    <meta name="description" content="Tutorials"/>
                    <link rel="shortcut icon" href="../favicon.ico"/>
                </Head>
                <body>
                <Main/>
                <NextScript/>
                </body>
            </Html>
        );
    }
}

export default MyDocument;
