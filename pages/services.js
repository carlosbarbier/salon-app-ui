import React from 'react';
import MainLayout from "../components/layouts/MainLayout";

const Services = () => {
    return (
        <MainLayout>
            <header className="bg-primary text-center py-5 mb-4">
                <div className="container">
                    <h1 className="fw-light text-white">Services we provide </h1>
                </div>
            </header>
            <div className="container">
                <div className="row">
                    <div className="col-lg-3 col-sm-6 mb-4">
                        <div className="card border-0 shadow rounded-xs pt-2">
                            <div className="card-body">
                                <i className="bi fw-bold bi-dice-5 text-primary" style={{fontSize:"60px"}}/>
                                <h4 className="mt-4 mb-3">Laser Hair Removal</h4>
                                <p>For what reason would it be advisable for me to think about business content?</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6 mb-4">
                        <div className="card border-0 shadow rounded-xs pt-2">
                            <div className="card-body">
                                <i className="bi fw-bold bi-bullseye text-primary" style={{fontSize:"60px"}}/>
                                <h4 className="mt-4 mb-3">Cosmetic Injections</h4>
                                <p>For what reason would it be advisable for me to think about business content?</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6 mb-4">
                        <div className="card border-0 shadow rounded-xs pt-2">
                            <div className="card-body">
                                <i className="bi fw-bold bi-aspect-ratio text-primary" style={{fontSize:"60px"}}/>
                                <h4 className="mt-4 mb-3">Body Sculpting</h4>
                                <p>For what reason would it be advisable for me to think about business content?</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6 mb-4">
                        <div className="card border-0 shadow rounded-xs pt-2">
                            <div className="card-body">
                                <i className="bi fw-bold bi-circle text-primary" style={{fontSize:"60px"}}/>
                                <h4 className="mt-4 mb-3">CoolSculpting</h4>
                                <p>For what reason would it be advisable for me to think about business content?</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6 mb-4">
                        <div className="card border-0 shadow rounded-xs pt-2">
                            <div className="card-body">
                                <i className="bi fw-bold bi-app-indicator text-primary" style={{fontSize:"60px"}}/>
                                <h4 className="mt-4 mb-3">Dermal Fillers</h4>
                                <p>For what reason would it be advisable for me to think about business content?</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6 mb-4">
                        <div className="card border-0 shadow rounded-xs pt-2">
                            <div className="card-body">
                                <i className="bi fw-bold bi-wallet text-primary" style={{fontSize:"60px"}}/>
                                <h4 className="mt-4 mb-3">Lip Fillers</h4>
                                <p>For what reason would it be advisable for me to think about business content?</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6 mb-4">
                        <div className="card border-0 shadow rounded-xs pt-2">
                            <div className="card-body">
                                <i className="bi fw-bold bi-puzzle text-primary" style={{fontSize:"60px"}}/>
                                <h4 className="mt-4 mb-3">Emsculpt</h4>
                                <p>For what reason would it be advisable for me to think about business content?</p>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-sm-6 mb-4">
                        <div className="card border-0 shadow rounded-xs pt-2">
                            <div className="card-body">
                                <i className="bi fw-bold bi-scissors text-primary" style={{fontSize:"60px"}}/>
                                <h4 className="mt-4 mb-3">Makeup Nails</h4>
                                <p>For what reason would it be advisable for me to think about business content?</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    );
};

export default Services;