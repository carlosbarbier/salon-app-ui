import MainLayout from "../components/layouts/MainLayout";
import NewBooking from "../components/booking/NewBooking";


export default function Home() {
    const description = `Trust is one of our core values and our aim is 
    to empower our clients, women and men, to become the best possible version of themselves
                                    `
    return (
        <MainLayout>
            <NewBooking sectionId="booking" description={description} title="Make Your reservation"/>
        </MainLayout>
    )
}
