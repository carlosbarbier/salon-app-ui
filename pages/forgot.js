import React, {useState} from 'react';
import MainLayout from "../components/layouts/MainLayout";
import {useForm} from "react-hook-form";
import {userForgotPasswordSchema} from "../validation";
import {forgotPassword, signInUser} from "../api/auth";
import ErrorComponent from "../components/common/ErrosComponent";
import {yupResolver} from "@hookform/resolvers/yup";
import {getErrors, isAuthenticated} from "../utils";
import Router from "next/router";
import LoadingButton from "../components/common/LoadingButton";
import Notification from "../components/common/Notification";


const ForgotPassword = () => {
    const [apiErrs, setApiErrors] = useState(null)
    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(userForgotPasswordSchema)});
    const [loading, setLoading] = useState(false)
    const [show, setShow] = useState(false)
    const onSubmit = async (data) => {
        const response = await forgotPassword(data);
        setLoading(true)

        if (response.status !== 200) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            setLoading(false)
            reset({email: ""})
            setShow(true)
            setTimeout(() => {
                Router.push("/reset")
            }, 3000)
        }

    };
    return (
        <MainLayout>
            <div className="container d-flex flex-column my-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-4">
                        <div className="card shadow border-0 py-5 ">
                            <div className="card-body p-6">
                                <div className="mb-4 text-center ">
                                    <h3 className="mb-3 fw-bold ">Forgot Password</h3>
                                </div>
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                    {show && <Notification type="success"
                                                           message="Check your email for the verification code"/>}
                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">Email</label>
                                        <input type="email"
                                               className="form-control"  {...register("email")}/>
                                        <span className="text-danger">{errors.email?.message}</span>
                                    </div>

                                    <div>
                                        <div className="d-grid ">
                                            <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                           message="Submit" load_message="Validating ..."/>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export default ForgotPassword;