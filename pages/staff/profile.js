import React from 'react';
import StaffLayout from "../../components/layouts/StaffLayout";
import ProfileComponent from "../../components/staff/ProfileComponent";
import {getCookie} from "../../utils";
import {getUser} from "../../api/auth";

const Profile = ({user}) => {


    return (
        <StaffLayout>
            <ProfileComponent user={user}/>
        </StaffLayout>

    );
};
Profile.getInitialProps = async (ctx) => {
    const token = getCookie('access_token', ctx.req);
    let user = null;
    if (token) {
        try {
            const response = await getUser()
            user = await response.data
        } catch (error) {
            if (error.response.status !== 200) {
                user = null;
            }
        }
    }
    if (user === null) {
        ctx.res.writeHead(302, {Location: '/login'});
        ctx.res.end();
    } else {
        return {user: user}
    }

}

export default Profile;