import React from 'react';
import StaffLayout from "../../components/layouts/StaffLayout";
import DashboardComponent from "../../components/staff/DashboardComponent";
import {getBookings} from "../../api/booking";
import {getCookie} from "../../utils";


const Home = ({bookings}) => {
    return (
        <StaffLayout>
            <DashboardComponent bookings={bookings}/>
        </StaffLayout>
    );
};

Home.getInitialProps = async (ctx) => {
    const token = getCookie('access_token', ctx.req);
    let bookings = null;
    if (token) {
        try {
            const response = await getBookings()
            bookings = await response.data
        } catch (error) {
            if (error.response.status !== 200) {
                bookings = null;
            }
        }
    }
    if (bookings === null) {
        ctx.res.writeHead(302, {Location: '/login'});
        ctx.res.end();
    } else {
        return {bookings: bookings}
    }

}

export default Home;
