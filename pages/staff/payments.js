import React from 'react';
import StaffLayout from "../../components/layouts/StaffLayout";
import {getCookie} from "../../utils";
import PaymentComponent from "../../components/staff/PaymentComponent";
import {getPayments} from "../../api/payment";

const Payments = ({payments}) => {
    return (
        <StaffLayout>
            <PaymentComponent payments={payments}/>
        </StaffLayout>

    );
};
Payments.getInitialProps = async (ctx) => {
    const token = getCookie('access_token', ctx.req);
    let payments = null;
    if (token) {
        try {
            const response = await getPayments()
            payments = await response.data
        } catch (error) {
            if (error.response.status !== 200) {
                payments = null;
            }
        }
    }
    if (payments === null) {
        ctx.res.writeHead(302, {Location: '/login'});
        ctx.res.end();
    } else {
        return {payments: payments}
    }

}

export default Payments;