import React, {useState} from 'react';
import MainLayout from "../components/layouts/MainLayout";
import {useForm} from "react-hook-form";
import {userLoginSchema} from "../validation";
import {signInUser} from "../api/auth";
import {getErrors, isAuthenticated} from "../utils";
import Router from "next/router";
import ErrorComponent from "../components/common/ErrosComponent";
import {yupResolver} from "@hookform/resolvers/yup";
import Link from "next/link";
import LoadingButton from "../components/common/LoadingButton";

const Login = () => {
    const [apiErrs, setApiErrors] = useState(null)
    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(userLoginSchema)});
    const [loading, setLoading] = useState(false)
    const onSubmit = async (data) => {
        setLoading(true)
        const response = await signInUser(data);
        if (response.status !== 200) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            const payload = await response.json()
            isAuthenticated(payload, () => {
                setLoading(false)
                reset({password: "", email: ""})
                Router.push("/staff/home")
            })
        }

    };
    return (
        <MainLayout>
            <div className="container d-flex flex-column  my-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-4">
                        <div className="card shadow border-0 mb-4 ">
                            <div className="card-body pb-3">
                                <div className="mb-4 text-center pt-3">
                                    <h3 className="mb-3 fw-bold ">Sign in</h3>
                                </div>
                                <form onSubmit={handleSubmit(onSubmit)}>
                                    {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                    <div className="mb-3">
                                        <label htmlFor="email" className="form-label">Email</label>
                                        <input type="email"
                                               className="form-control"  {...register("email")}/>
                                        <span className="text-danger">{errors.email?.message}</span>
                                    </div>
                                    <div className="mb-3">
                                        <label htmlFor="password" className="form-label">Password</label>
                                        <input type="password"
                                               className="form-control"  {...register("password")} />
                                        <span className="text-danger">{errors.password?.message}</span>
                                    </div>
                                    <div className="d-lg-flex justify-content-between align-items-center mb-4">
                                        <div>
                                            <Link href="/forgot">
                                                <a className="text-black" style={{fontSize: "12px"}}>Forgot your
                                                    password?</a>
                                            </Link>

                                        </div>
                                    </div>
                                    <div>
                                        <div className="d-grid ">
                                            <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                           message="Login" load_message="Login ..."/>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </MainLayout>
    )
}

export default Login;