const path = require("path");
const withAntdLess = require('next-plugin-antd-less');
module.exports = withAntdLess({
    reactStrictMode: true,
    sassOptions: {
        includePaths: [path.join(__dirname, 'styles')]
    },
    modifyVars: {
        '@primary-color': '#0055d4',
        '@border-radius-base': '0.25em'
    },
    lessVarsFilePathAppendToEndOfContent: false,
    cssLoaderOptions: {},
    async rewrites() {
        return [
            {
                source: '/api/:path*',
                destination: 'https://ehx9rdhln3.execute-api.us-east-1.amazonaws.com/:path*'
            },
            {
                source: '/api/:path*',
                destination: 'https://vhqogy978b.execute-api.us-east-1.amazonaws.com/:path*'
            }
        ]
    },
    future: {
        webpack5: true
    },
    eslint: {
        ignoreDuringBuilds: true,
    },
    images: {
        domains: ['source.unsplash.com']
    }
});
