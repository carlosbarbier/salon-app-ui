import {PAYMENT_URL} from "../constants";
import axios from "axios";


export const proceedPayment = async (payment) => {
    try {
        const response = await fetch(`${PAYMENT_URL}/payments`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(payment),
        });
        return await response;
    } catch (error) {

    }
};


export const getPayments = async () => {
    return await axios.get(`${PAYMENT_URL}/payments`, {
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    })
};
