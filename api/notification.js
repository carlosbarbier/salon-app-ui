import {NOTIFICATION_URL} from "../constants";


export const sendNotification = async (notification) => {
    try {
        const response = await fetch(`${NOTIFICATION_URL}/notifications`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(notification),
        });
        return await response;
    } catch (error) {

    }
};
