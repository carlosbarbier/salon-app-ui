
import {getCookie, removeCookie, removeFromLocalstorage, setCookie, storeToLocalstorage} from "../utils";
import {AUTH_URL, BOOKING_URL} from "../constants";
import axios from "axios";


export const registerUser = async (user) => {
    try {
        const response = await fetch(`${AUTH_URL}/register`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(user),
        });
        return await response;
    } catch (error) {

    }
};

export const signInUser = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/login`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;
    } catch (error) {

    }
};

export const logout = async (next) => {
    try {
        await fetch(`${AUTH_URL}/logout`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("access_token"),
            },
            method: "POST",
        });
        removeCookie("access_token");
        removeFromLocalstorage("user");
        removeFromLocalstorage("access_token");
        removeFromLocalstorage("expires_in");
        removeFromLocalstorage("refresh_token");
        next()

    } catch (error) {

    }
};


export const getUser = async () => {
    return await axios.get(`${AUTH_URL}/me`, {
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
            Authorization: "Bearer " + getCookie("access_token"),
        },
    })
};
export const confirmEmailAccount = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/email/confirmation`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;

    } catch (error) {

    }
};
export const forgotPassword = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/password/forgot`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;

    } catch (error) {

    }
};

export const resendCode = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/resend/code`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;

    } catch (error) {

    }
};

export const changePassword = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/password/change`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("access_token"),
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;

    } catch (error) {

    }
};

export const resetPassword = async (data) => {
    try {
        const response = await fetch(`${AUTH_URL}/password/reset`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(data),
        });
        return await response;

    } catch (error) {

    }
};