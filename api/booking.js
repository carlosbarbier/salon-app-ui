import {getCookie} from "../utils";
import {BOOKING_URL} from "../constants";
import axios from "axios";


export const saveNewBooking = async (booking) => {
    try {
        const response = await fetch(`${BOOKING_URL}/bookings`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
            },
            method: "POST",
            body: JSON.stringify(booking),
        });
        return await response;
    } catch (error) {

    }
};

export const getBooking = async (bookingId) => {
    try {
        const booking = await fetch(`${BOOKING_URL}/bookings/${bookingId}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("access_token"),
            }
        });
        return await booking.json()
    } catch (error) {

    }
};

export const getBookings = async () => {
    return await axios.get(`${BOOKING_URL}/bookings`, {
        headers: {
            Accept: "application/json",
            "Content-Type": "application/json",
        },
    })
};


export const deleteBooking = async (bookingId) => {
    try {
        const response = await fetch(`${BOOKING_URL}/bookings/${bookingId}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("access_token"),
            },
            method: "DELETE",
        });
        return await response;

    } catch (error) {

    }
};

export const updateBooking = async (bookingId, bookingDto) => {
    try {
        const response = await fetch(`${BOOKING_URL}/bookings/${bookingId}`, {
            headers: {
                Accept: "application/json",
                "Content-Type": "application/json",
                Authorization: "Bearer " + getCookie("access_token"),
            },
            method: "PUT",
        });
        return await response

    } catch (error) {

    }
};