import * as Yup from "yup";

export const userRegisterSchema = Yup.object().shape({
    name: Yup.string()
        .required('Name is required'),
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
    password: Yup.string()
        .min(8, 'Password must be at least 8 characters')
        .required('Password is required'),
});

export const changePasswordSchema = Yup.object().shape({
    password_confirmation: Yup.string()
        .oneOf([Yup.ref('new_password'), null], 'Passwords must match'),
    current_password: Yup.string()
        .required('Password is required'),
    new_password: Yup.string()
        .min(8, 'Password must be at least 8 characters')
        .required('Password is required'),
});

export const resetPasswordSchema = Yup.object().shape({
    confirm_code: Yup.string()
        .required('Code required'),
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
    password: Yup.string()
        .min(8, 'Password must be at least 8 characters')
        .required('Password is required'),
});

export const changeProfileSchema = Yup.object().shape({
    name: Yup.string()
        .required('Name is required'),
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),

});

export const userLoginSchema = Yup.object().shape({
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
    password: Yup.string()
        .required('Password is required'),
});

export const emailSchema = Yup.object().shape({
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
});

export const userConfirmationSchema = Yup.object().shape({
    code: Yup.string()
        .required('Confirmation code is required')
        .matches(/^\d+/, 'Code must be a number')
});

export const userForgotPasswordSchema = Yup.object().shape({
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid')
});

export const newBookingSchema = Yup.object().shape({
    email: Yup.string()
        .required('Email is required')
        .email('Email is invalid'),
    name: Yup.string()
        .required('Full name is required'),

    date: Yup.string()
        .required('valid date is required'),

    time: Yup.string()
        .required('valid time is required'),

    service: Yup.string()
        .required('valid service is required')

});
export const cartSchema = Yup.object().shape({
    expiry_date: Yup.string()
        .matches(/([0-9]{2})\/([0-9]{2})/, 'Not a valid expiration date')
        .required('Expiration date'),
    cvc: Yup.string()
        .matches(/^\d+/, 'Cvc must be a number')
        .required('Cvc is required'),
    card_number: Yup.string()
        .required('Credit Payment number required'),
});

