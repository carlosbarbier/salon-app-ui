export const AUTH_URL = "https://uqr8ar9kei.execute-api.us-east-1.amazonaws.com/api/users"
export const BOOKING_URL = "https://fzpjprk3qd.execute-api.us-east-1.amazonaws.com/api"
export const PAYMENT_URL = "https://ut1tilpjn3.execute-api.us-east-1.amazonaws.com/api"
export const NOTIFICATION_URL = "https://56xlqt3z8f.execute-api.us-east-1.amazonaws.com/api"
export const SERVICES = [
    {
        id: 1,
        name: "Hair Extension",
        price: 60
    },
    {
        id: 2,
        name: "Barber",
        price: 15
    },
    {
        id: 3,
        name: "Nails",
        price: 20
    },
    {
        id: 4,
        name: "Makeup",
        price: 50
    },
    {
        id: 5,
        name: "Hairs Cuts",
        price: 25
    },
    {
        id: 6,
        name: "Massage",
        price: 50
    }
]
