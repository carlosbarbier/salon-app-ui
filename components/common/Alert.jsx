import React from 'react';

const Alert = ({type, message}) => {
    return (
        <div className={`alert ${type} d-flex justify-content-between`} role="alert">
            {message}
            <button type="button" className="btn-close border-0" data-bs-dismiss="alert" aria-label="Close"/>
        </div>
    );
};

export default Alert;