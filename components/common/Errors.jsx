import React from 'react';

const Errors = ({errors}) => {
    return (
        <div>
            {(errors || []).map((error, index) => (
                <div key={index} style={{color: "#f5222d"}}>
                    {error.message}
                </div>
            ))}
        </div>
    );
};

export default Errors;