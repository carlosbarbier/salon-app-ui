import React from 'react';

const ErrorComponent = ({errors}) => {
    return (
        <>
            {(errors || []).map((error, index) => (
                <div key={index} className="mb-2">
                    <span className="text-danger">
                        {error}
                    </span>
                </div>
            ))}
        </>
    );
};

export default ErrorComponent;