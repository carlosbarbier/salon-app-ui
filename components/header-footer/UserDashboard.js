import React from 'react';
import Link from "next/link";
import Router from "next/router";
import {logout} from "../../api/auth";

const UserDashboard = ({url}) => {
    return (
        <div className="collapse navbar-collapse logout">
            <div className="ms-auto mt-3 mt-lg-0">
                <Link href={`${url}`}>
                    <a className="btn btn-link text-black text-decoration-none shadow-sm me-1 fw-bold" >Dashboard</a>
                </Link>
                <a className="btn btn-primary  me-1 fw-bold logout"
                   onClick={() => logout(() => Router.replace("/login"))}>
                    Logout
                </a>
            </div>
        </div>
    );
};

export default UserDashboard;