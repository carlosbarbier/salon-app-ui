import React from 'react';
import Link from "next/link";
import Image from "next/image";
import {getUser, isLogin} from "../../utils";
import UserDashboard from "./UserDashboard";



const Header = () => {
    const user = getUser()
    return (
        <div id="header">
            <nav className="navbar navbar-expand-lg">
                <div className="container">
                    <div className="d-flex flex-grow-1">
                        <Link href="/">
                            <a>
                                <span className="w-100 d-lg-none d-block"/>
                                <Image src="/salon-logo.png" alt="logo" width="225" height="37"/>
                            </a>

                        </Link>

                    </div>
                    <div className="collapse navbar-collapse flex-grow-1 text-right me-5 ">
                        <ul className="navbar-nav ms-auto flex-nowrap">
                            <li className="nav-item">
                                <Link href="/services"
                                      as={`/services`}>
                                    <a className="nav-link m-2 menu-item fw-bold">Services</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/specialists"
                                      as={`/specialists`}>
                                    <a className="nav-link m-2 menu-item fw-bold">Specialists</a>
                                </Link>
                            </li>
                            <li className="nav-item">
                                <Link href="/about"
                                      as={`/about`}>
                                    <a className="nav-link m-2 menu-item fw-bold">About</a>
                                </Link>
                            </li>
                        </ul>
                    </div>
                    {!isLogin() &&
                        <div className="collapse navbar-collapse authentication">
                            <div className="ms-auto mt-3 mt-lg-0">
                                <Link href="/login">
                                    <a className="btn btn-white shadow-sm me-1 fw-bold">Sign In</a>
                                </Link>

                                <Link href="/register">
                                    <a className="btn btn-primary fw-bold btn_register">Staff Registration</a>
                                </Link>
                            </div>
                        </div>
                    }
                    {user &&  <UserDashboard url="/staff/home"/>}
                </div>
            </nav>
        </div>


    );
};

export default Header;