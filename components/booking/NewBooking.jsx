import React, {useState} from 'react';
import {useForm} from "react-hook-form";

import {cartSchema, newBookingSchema} from "../../validation";
import LoadingButton from "../common/LoadingButton";
import ErrorComponent from "../common/ErrosComponent";
import {yupResolver} from "@hookform/resolvers/yup";
import Notification from "../common/Notification";
import {saveNewBooking} from "../../api/booking";
import {getErrors} from "../../utils";
import {Divider} from "antd";
import {proceedPayment} from "../../api/payment";
import {SERVICES} from "../../constants";
import {sendNotification} from "../../api/notification";

const NewBooking = ({sectionId, description, title}) => {
    const [loading, setLoading] = useState(false)
    const [loadingPayment, setLoadingPayment] = useState(false)
    const [confirmed, setConfirmed] = useState(false)
    const [showCard, setShowCard] = useState(false)
    const [apiErrs, setApiErrors] = useState(null)
    const [payErrs, setPayErrors] = useState(null)
    const [show, setShow] = useState(false)
    const [service, setService] = useState(null)
    const [bookingId, setBookingId] = useState("")
    const [newBooking, setNewBooking] = useState(null)
    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(newBookingSchema)});
    const {
        register: registerPayment,
        handleSubmit: handleSubmitPayment,
        reset: resetPayment,
        formState: {errors: paymentErrors}
    } = useForm({resolver: yupResolver(cartSchema)});

    const onSubmit = async (payload) => {
        setLoading(true)
        const booking = {
            booking_date: payload.date.slice(),
            service: payload.service,
            booking_time: payload.time,
            full_name: payload.name,
            email: payload.email,

        }
        const found = findService(payload.service)
        setService(found)

        const response = await saveNewBooking(booking);

        if (response.status !== 201) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            const {booking_id} = await response.json()
            setNewBooking({
                booking_date: payload.date.slice(),
                service: payload.service,
                booking_time: payload.time,
                name: payload.name,
                email: payload.email,
            })
            setBookingId(booking_id)
            reset({name: "", email: "", date: "dd/mm/yyyy"})
            setLoading(false)
            setShow(true)
            setConfirmed(true)
        }
    };

    const handleYes = () => {
        setShowCard(true)
    }

    const handleNo = async () => {
        setShowCard(false)
        setConfirmed(false)
        const notification = {...newBooking, booking_id: bookingId, receipt: null}
        await sendNotification(notification)

    }

    const onSubmitPayment = async (payload) => {
        setLoadingPayment(true)
        const exp_month = payload.expiry_date.slice(0, 2)
        const exp_year = "20" + payload.expiry_date.slice(-2)

        const data = {
            card_number: payload.card_number,
            exp_month,
            exp_year,
            cvc: payload.cvc,
            price: service.price || 12,
            service_name: service.name || "Hairs Cuts"
        }

        const response = await proceedPayment(data);

        if (response.status !== 200) {
            const err = await getErrors(response)
            setPayErrors(err)
            setLoadingPayment(false)
        } else {

            const {receipt} = await response.json()
            const notification = {...newBooking, booking_id: bookingId, receipt}
            await sendNotification(notification)

            resetPayment({expiry_date: "", cvc: "", card_number: ""})
            setLoadingPayment(false)
            setShowCard(false)
            setConfirmed(false)
        }


    }

    const handleCancel = async () => {
        setShowCard(false)
        setConfirmed(false)
        const notification = {...newBooking, booking_id: bookingId, receipt: null}
        await sendNotification(notification)

    }

    const findService = (serviceName) => {
        return SERVICES.find(service => service.name === serviceName)
    }

    return (
        <div id={sectionId} className="section">
            <div className="section-center">
                <div className="container">
                    <div className="row">
                        <div className="col-md-7 col-md-push-5">
                            <div className="booking-cta">
                                <h1>{title}</h1>
                                <p style={{fontSize: "37px", lineHeight: "1.1"}} className="mt-4">
                                    {description}<span className="fw-bold fs-1 text-primary">.</span>
                                </p>
                            </div>
                        </div>
                        <div className="col-md-4 col-md-pull-7">
                            <>
                                {!confirmed &&
                                    <div className="booking-form">
                                        {show && <Notification type="success"
                                                               message="Booking Confirmed, check your email for details."/>}
                                        <form onSubmit={handleSubmit(onSubmit)}>
                                            {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                            <div className="form-group">
                                                <label htmlFor="password" className="form-label">Name</label>
                                                <input type="text" className="form-control"
                                                       name="name" {...register("name")}
                                                       placeholder="Enter your full name"/>
                                                <span className="text-danger">{errors.name?.message}</span>
                                            </div>
                                            <div className="form-group">
                                                <label htmlFor="email" className="form-label">Email</label>
                                                <input type="email"
                                                       className="form-control" {...register("email")}
                                                       placeholder="Enter Your email"/>
                                                <span className="text-danger">{errors.email?.message}</span>
                                            </div>

                                            <div className="form-group">
                                                <span className="form-label">Service</span>
                                                <select className="form-control" {...register("service")}
                                                        style={{textIndent: "4px"}}>
                                                    <option disabled>Select Your service</option>
                                                    {SERVICES && SERVICES.map(service => (
                                                        <option key={service.id}
                                                                value={service.name}>{service.name}</option>
                                                    ))}
                                                </select>
                                                <span className="select-arrow"/>
                                                <span className="text-danger">{errors.service?.message}</span>
                                            </div>
                                            <div className="row">
                                                <div className="col-sm-7">
                                                    <div className="form-group">
                                                        <span className="form-label">Date</span>
                                                        <input className="form-control" {...register("date")}
                                                               pattern="\d{4}-\d{2}-\d{2}" type="date"/>
                                                        <span className="text-danger">{errors.date?.message}</span>
                                                    </div>
                                                </div>
                                                <div className="col-sm-5">
                                                    <div className="form-group">
                                                        <span className="form-label">Time</span>
                                                        <select className="form-control" {...register("time")}
                                                                style={{textIndent: "4px"}}>
                                                            <option disabled>Time</option>
                                                            <option value="8AM - 9AM">8AM - 9AM</option>
                                                            <option value="9AM - 10AM">9AM - 10AM</option>
                                                            <option value="10AM - 11AM">10AM - 11AM</option>
                                                            <option value="11AM - 12AM">11AM - 12AM</option>
                                                            <option value="12PM - 13PM">12PM - 13PM</option>
                                                            <option value="13PM - 14PM">13PM - 14PM</option>
                                                            <option value="14PM- 15PM">14PM- 15PM</option>
                                                            <option value="15PM - 16PM">15PM - 16PM</option>
                                                            <option value="16PM - 17PM">16PM - 17PM</option>
                                                        </select>
                                                        <span className="select-arrow"/>
                                                        <span className="text-danger">{errors.time?.message}</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-btn">
                                                <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                               message="Book Now" load_message="Saving Booking ..."/>
                                            </div>
                                        </form>

                                    </div>
                                }
                                {show && confirmed && !showCard &&
                                    <div
                                        className="payment-confirmation booking-form bg-white border-0 my-3 position-relative"
                                        style={{minHeight: "500px"}}>
                                        <div className="position-absolute top-50 start-50 translate-middle">
                                            <p className="text-center">Would you like to make the payment now?</p>
                                            <div
                                                className="d-grid gap-2 d-md-flex justify-content-md-center  ">
                                                <button className="btn btn-secondary mx-2" type="button"
                                                        onClick={handleYes}>Yes
                                                </button>
                                                <button className="btn btn-danger mx-2" type="button"
                                                        onClick={handleNo}>No
                                                </button>
                                            </div>
                                        </div>

                                    </div>
                                }
                            </>
                            {showCard &&

                                <div className="row">
                                    <div className="col-lg-12">
                                        {payErrs && <ErrorComponent errors={payErrs}/>}
                                        <div className="card py-5 px-3  bg-white border-0 rounded-3">
                                            <form onSubmit={handleSubmitPayment(onSubmitPayment)}>
                                                <div className="mb-3">
                                                    <label htmlFor="cart_number" className="form-label">Card
                                                        Number</label>
                                                    <input type="text"
                                                           className="form-control"  {...registerPayment("card_number")}
                                                           minLength={16} maxLength={16}/>
                                                    <span
                                                        className="text-danger">{paymentErrors.card_number?.message}</span>
                                                </div>
                                                <div className="row my-5">
                                                    <div className="col-md-6">
                                                    <span
                                                        className="font-weight-normal card-text">Expiry Date(mm/yy)</span>
                                                        <div className="input">
                                                            <input type="text"
                                                                   className="form-control" {...registerPayment("expiry_date")}
                                                                   minLength={5} maxLength={5}/>
                                                            <span
                                                                className="text-danger">{paymentErrors.expiry_date?.message}</span>
                                                        </div>
                                                    </div>
                                                    <div className="col-md-6 "><span
                                                        className="font-weight-normal card-text">CVC/CVV</span>
                                                        <div className="input">
                                                            <input type="text"
                                                                   className="form-control" {...registerPayment("cvc")}
                                                                   minLength={3} maxLength={3}/>
                                                            <span
                                                                className="text-danger">{paymentErrors.cvc?.message}</span>
                                                        </div>
                                                    </div>

                                                </div>
                                                <div className="d-grid py-3">
                                                    <LoadingButton loading={loadingPayment}
                                                                   css_class="btn btn-secondary"
                                                                   message={`Pay for ${service && service.name}
                                                        : €${service && service.price || 29}`}
                                                                   load_message="Processing Payment"/>
                                                </div>

                                            </form>
                                            <Divider style={{fontSize: "12px"}}/>
                                            <div className="text-center">
                                                <div className="d-grid py-1">
                                                    <button className="btn btn-outline-danger"
                                                            onClick={handleCancel}>Cancel
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default NewBooking;