import React from 'react';

const ChangeProfile = (user) => {

    return (
        <div className="bg-white p-3">
            <div className="container d-flex flex-column  my-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-12">
                        <div className="card-body pb-5">
                            <div className="mb-3">
                                <label htmlFor="email" className="form-label">Email</label>
                                <input type="email" className="form-control" disabled={true}
                                       defaultValue={user && user.email}/>

                            </div>
                            <div className="mb-3">
                                <label htmlFor="name" className="form-label">Name</label>
                                <input type="text" className="form-control" defaultValue={user && user.name}
                                       disabled={true}/>

                            </div>
                            <div>
                                <div className="d-grid pb-3 my-5">
                                    <button type="submit" className="btn btn-secondary disabled ">Update Profile
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default ChangeProfile;