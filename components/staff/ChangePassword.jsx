import React, {useState} from 'react';
import ErrorComponent from "../common/ErrosComponent";
import {useForm} from "react-hook-form";

import {changePasswordSchema} from "../../validation";
import {changePassword} from "../../api/auth";
import {getErrors} from "../../utils";
import {yupResolver} from "@hookform/resolvers/yup";
import LoadingButton from "../common/LoadingButton";
import Router from "next/router";

const ChangePassword = () => {
    const [apiErrs, setApiErrors] = useState(null)
    const [loading, setLoading] = useState(false)
    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(changePasswordSchema)});

    const onSubmit = async (data) => {
        setLoading(true)
        const payload = {
            previous_password: data.current_password,
            new_password: data.new_password,
            password_confirmation: data.password_confirmation
        }
        const response = await changePassword(payload);
        if (response.status !== 200) {
            const err = await getErrors(response)
            setApiErrors(err)
            setLoading(false)
        } else {
            setLoading(false)
            reset({current_password: "", new_password: "", password_confirmation: ""})
            Router.push("/staff/home")
        }
    };
    return (
        <div className="bg-white p-3">
            <div className="container d-flex flex-column  my-5">
                <div className="row align-items-center justify-content-center g-0 ">
                    <div className=" col-md-12">
                        <div className="card-body pb-5">
                            <form onSubmit={handleSubmit(onSubmit)}>
                                {apiErrs && <ErrorComponent errors={apiErrs}/>}
                                <div className="mb-3">
                                    <label htmlFor="current_password" className="form-label">Current Password</label>
                                    <input type="password"
                                           className="form-control"  {...register("current_password")}/>
                                    <span className="text-danger">{errors.current_password?.message}</span>
                                </div>
                                <div className="mb-3">
                                    <label htmlFor="new_password" className="form-label">New Password</label>
                                    <input type="password"
                                           className="form-control"  {...register("new_password")} />
                                    <span className="text-danger">{errors.new_password?.message}</span>
                                </div>

                                <div className="mb-3">
                                    <label htmlFor="name" className="form-label">Confirm Password</label>
                                    <input type="password"
                                           className="form-control"  {...register("password_confirmation")} />
                                    <span className="text-danger">{errors.password_confirmation?.message}</span>
                                </div>
                                <div>
                                    <div className="d-grid pb-3 my-5">
                                        <div className="d-grid ">
                                            <LoadingButton loading={loading} css_class="btn btn-secondary"
                                                           message="Change Password" load_message="Updating Password"/>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default ChangePassword;