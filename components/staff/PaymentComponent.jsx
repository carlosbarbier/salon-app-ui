import React from 'react';
import Link from "next/link";

const PaymentComponent = ({payments}) => {
    return (
        <div className="invoice-wrapper bg-white ">
            <div className="row">
                <div className="col-md-12">

                    <div className="bg-white p-3">
                        <table className="table">
                            <thead>
                            <tr>
                                <th scope="col" className="fw-normal text-muted">Invoice Id </th>
                                <th scope="col" className="fw-normal text-muted">Issue Date</th>
                                <th scope="col" className="fw-normal text-muted">Total</th>
                                <th scope="col" className="fw-normal text-muted">Receipt</th>
                            </tr>
                            </thead>
                            <tbody>
                            {payments && payments.map(payment => (
                                <tr key={payment.id}>
                                    <th className="fw-normal">{payment.id}</th>
                                    <td>{payment.payment_date}</td>
                                    <td>€{payment.amount}</td>
                                    <td>
                                        <Link href={payment.receipt_url} passHref>
                                            <a target="_blank" rel="noopener noreferrer" className="link-dark"> <span><i
                                                className="bi bi-download"/> Receipt</span></a>
                                        </Link>
                                    </td>
                                </tr>
                            ))}
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>
        </div>
    );
};

export default PaymentComponent;