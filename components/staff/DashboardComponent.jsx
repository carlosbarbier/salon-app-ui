import React from 'react';

const DashboardComponent = ({bookings}) => {

    return (
        <div className="card bg-white border-0 rounded-0 dashboard-content py-5 px-5">
            <p className="h4 mb-4">The Bookings</p>
            <table className="table table-bordered">
                <thead>
                <tr>
                    <th>#</th>
                    <th>Full Name</th>
                    <th>Email</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Service</th>
                </tr>
                </thead>
                <tbody>
                {bookings && bookings.map((booking, index) => (
                    <tr key={index}>
                        <td>{index + 1}</td>
                        <td>{booking.full_name}</td>
                        <td>{booking.email}</td>
                        <td>{booking.booking_date}</td>
                        <td>{booking.booking_time}</td>
                        <td>{booking.service}</td>
                    </tr>))}

                </tbody>
            </table>

        </div>


    );
};

export default DashboardComponent;