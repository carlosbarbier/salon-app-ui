import React from 'react';

const StaffContentArea = ({children}) => {
    return (
        <>
            {children}
        </>
    );
};

export default StaffContentArea;