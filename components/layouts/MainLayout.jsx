import Header from "../header-footer/Header";
import Footer from "../header-footer/Footer";

const MainLayout = ({children}) => {
    return (
        <div>
            <Header/>
            <main >
                {children}
            </main>

            <Footer/>
        </div>
    );
};

export default MainLayout;