import React, {useState} from 'react';
import {useForm} from "react-hook-form";
import {cartSchema} from "../validation";
import ErrorComponent from "../components/common/ErrosComponent";
import LoadingButton from "../components/common/LoadingButton";
import {yupResolver} from "@hookform/resolvers/yup";
import {Divider} from "antd";
import Link from "next/link";

const CreditCard = () => {

    const [loading, setLoading] = useState(false)
    const [apiErrs, setApiErrors] = useState(null)

    const {register, handleSubmit, reset, formState: {errors}} = useForm({resolver: yupResolver(cartSchema)});

    const onSubmit = async (body) => {
        console.log(body)
        setLoading(true)
        const exp_month = body.expiry_date.slice(0, 2)
        const exp_year = "20" + body.expiry_date.slice(-2)
    };

    return (

        <div className="row mt-5">
            <div className="col-lg-5 mx-auto">
                {apiErrs && <ErrorComponent errors={apiErrs}/>}
                <div className="card py-5 px-3  bg-white border-0 rounded-3">
                    <form onSubmit={handleSubmit(onSubmit)}>
                        <div className="mb-3">
                            <label htmlFor="cart_number" className="form-label">Card
                                Number</label>
                            <input type="text"
                                   className="form-control"  {...register("card_number")}/>
                            <span className="text-danger">{errors.card_number?.message}</span>
                        </div>
                        <div className="row my-5">
                            <div className="col-md-6">
                                                    <span
                                                        className="font-weight-normal card-text">Expiry Date(mm/yy)</span>
                                <div className="input">
                                    <input type="text"
                                           className="form-control" {...register("expiry_date")}/>
                                    <span
                                        className="text-danger">{errors.expiry_date?.message}</span>
                                </div>
                            </div>
                            <div className="col-md-6 "><span
                                className="font-weight-normal card-text">CVC/CVV</span>
                                <div className="input">
                                    <input type="text"
                                           className="form-control" {...register("cvc")}/>
                                    <span className="text-danger">{errors.cvc?.message}</span>
                                </div>
                            </div>

                        </div>
                        <div className="d-grid py-3">
                            <LoadingButton loading={loading} css_class="btn btn-secondary"
                                           message={`Pay Now
                                                        £ ${12}`} load_message="Processing Payment"/>
                        </div>

                    </form>
                </div>
                <Divider style={{fontSize: "12px"}}>Resend Code</Divider>
                <div className="mt-3 text-center">
                    <div className="d-grid py-1">
                            <button className="btn btn-outline-primaryregister_btn">Cancel</button>
                    </div>
                </div>
            </div>
        </div>

    )
}

export default CreditCard;