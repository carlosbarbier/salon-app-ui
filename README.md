# Salon Booking App User Interface

Application url [URL](https://main.d3h0ze9vuyysfe.amplifyapp.com)

## Requirements

Have Node Js installed in your machine(version 16)

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.


## Hosting

AWS Amplify Hosting.
